# FPO RegEx Everything


### A. Quy định
- Chào đón tất cả commit từ tất cả mọi người
- Sau mỗi commit mới hãy tạo một Merge request để Ban quản trị có thể merge vào branch **master**


### B. Mục lục

#### 1. Email
```javascript
/**
 * Expose email regex.
 *
 * Example input:
 *   yo+3@gmail.com
 *   tobi@ferret.com
 *   stack@lebron.technology
 */

module.exports = /^([\w_\.\-\+])+\@([\w\-]+\.)+([\w]{2,10})+$/;
```

#### 2. Phone
```javascript
'use strict';

module.exports = function(options) {
  options = options || {};
  var regexBase = '(?:\\+?(\\d{1,3}))?[-. (]*(\\d{3})[-. )]*(\\d{3})[-. ]*(\\d{4})(?: *x(\\d+))?';
  var indianRegexBase = '(?:(?:\\+|0{0,2})91(\\s*[\\ -]\\s*)?|[0]?)?[789]\\d{9}|(\\d[ -]?){10}\\d';

  return options.indian ? options.exact ? new RegExp('^' + indianRegexBase + '$') :
                         new RegExp('\\s*' + indianRegexBase + '\\s*', 'g') : options.exact ? new RegExp('^' + regexBase + '$') :
                         new RegExp('\\s*' + regexBase + '\\s*', 'g');
}
```
- Regex from http://stackoverflow.com/a/16702965/1378668.

#### 3. Username
Which usernames to allow typically varies between applications. For prototypes however it's nice to have an off the shelf solution. This module is that solution. It follows the same rules GitHub uses:

> Username may only contain alphanumeric characters or single hyphens, and cannot begin or end with a hyphen.

```javascript
/**
 * Expose username regex, following github conventions
 * like:
 * _Username may only contain alphanumeric characters
 * and only single hyphens, and cannot begin or end with a hyphen._
 *
 *
 * Example input:
 *   foo
 *   foo-bar
 */
module.exports = function regexUsername () {
  return /^([a-z\d]+-)*[a-z\d]+$/i;
};
```

